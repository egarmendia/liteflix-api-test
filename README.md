# litebox-api-test

# Caso

El proyecto consiste en un Catálogo de Películas dinámico. Las películas pueden obtenerse de una API pública así como también de un repositorio privado, las mismas deben estar categorizadas.

Se espera, de esta manera, que el Catálogo de Películas final, liste las películas que provienen de la API pública + Las peliculas que agregue el usuario a traves de la API de Liteflix.

El prototipo de diseño se encuentra disponible en [Zeplin](https://zpl.io/VQRNKx4) a modo de referencia. El maquetado del sitio no forma parte del desafío.


# Objetivo

El objetivo de este desafío es crear una API en NodeJS/Laravel que disponga de los siguientes endpoints de autenticación (estas pantallas no se encuentran diseñadas) y de películas (Middleware con API Pública + Catálogo Privado):

Auth Endpints:
- Login
- Social Login (Google + Facebook)
- Signup
- Logout
- Password Recovery

Movie Endpoints:
- Now Playing Movie (middleware con la api publica que devuelva solo los datos necesarios en los diseños)
- Upcoming Movies (middleware con la api publica que devuelva solo los datos necesarios en los diseños)
- Popular Movies (middleware con la api publica que devuelva solo los datos necesarios en los diseños)
- Upload Movies (subir peliculas, la info a subir es la misma que se devuelve para cada película en los endpoints anteriores)
- My Movies (lista peliculas agregadas con endpont anterior)
- Categories (lista de categorias que aparecen en el select del modal para subir pelicula)


Notas:
- No hay directivas estrictas, la solución debería ser lo más performante y robusta posible a criterio del desarrollador.
- La elección del motor de base de datos queda a criterio del desarrollador.

## Public API Connection

**Get Now Playing**

[GET] https://api.themoviedb.org/3/movie/now_playing?api_key=6f26fd536dd6192ec8a57e94141f8b20

**Get Upcoming**

[GET] https://api.themoviedb.org/3/movie/upcoming?api_key=6f26fd536dd6192ec8a57e94141f8b20

**Get Popular**

[GET] https://api.themoviedb.org/3/movie/popular?api_key=6f26fd536dd6192ec8a57e94141f8b20

- Información acerca del uso de imágenes:
https://developers.themoviedb.org/3/getting-started/images


# Entrega

Una vez finalizado el ejercicio, deberás compartir un repositorio en Git para poder evaluarlo. (Suma un montón si podés deployarlo en zeit / heroku o similares)


# Criterios de Evaluación

- Puntualidad en la entrega
- Clean Code
- Performance de la API
- Correcto funcionamiento de la API